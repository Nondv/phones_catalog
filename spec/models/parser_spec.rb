require 'rails_helper'

RSpec.describe Parser, type: :model do
  context '#brands method result' do
    before :each do
      @brands = Parser.brands
      @brand_names = @brands.map(&:name)
    end

    it('is array') { expect(@brands).to be_a Array }
    it('is not empty') { expect(@brands).not_to be_empty }

    #
    # some tests for checking if it parses from gsmarena
    #
    it('contains 108 elements') { expect(@brands.size).to eq 108 }
    it 'contains some brands by name' do
      expect(@brand_names).to include('Acer')
      expect(@brand_names).to include('Fujitsu Siemens')
      expect(@brand_names).to include('ZTE')
    end

    it 'sets gsmarena_url' do
      acer = @brands.find { |b| b.name == 'Acer' }
      expect(acer.gsmarena_url).to eq 'http://www.gsmarena.com/acer-phones-59.php'

      zte = @brands.find { |b| b.name == 'ZTE' }
      expect(zte.gsmarena_url).to eq 'http://www.gsmarena.com/zte-phones-62.php'
    end
  end

  context '#models method result (Apple stub)' do
    before :each do
      @apple = Brand.new name: 'Apple',
                         gsmarena_url: 'http://www.gsmarena.com/apple-phones-48.php'

      @model_list = Parser.models(@apple)
      @model_names = @model_list.map(&:name)
    end

    it('is array') { expect(@model_list).to be_a Array }
    it('contains 38 elements') { expect(@model_list.size).to eq 38 }
    it('contains "iPhone 5C"') { expect(@model_names).to include 'iPhone 5c' }
    it('contains "iPad Pro 9.7"') { expect(@model_names).to include 'iPad Pro 9.7' }

    it 'sets gsmarena_url' do
      iphone_6s_plus = @model_list.find { |m| m.name == 'iPhone 6s Plus' }
      expect(iphone_6s_plus.gsmarena_url).to eq 'http://www.gsmarena.com/apple_iphone_6s_plus-7243.php'
    end
  end

  context '#model_info method result (gsmarena iPhone 6s+ stub)' do
    before :each do
      @model = Model.new(name: 'iPhone 6s Plus', gsmarena_url: 'http://www.gsmarena.com/apple_iphone_6s_plus-7243.php')
      @info = Parser.model_info(@model)
    end

    it('is hash') { expect(@info).to be_a Hash }
    it('contains 13 keys') { expect(@info.size).to eq 13 }
    it('contains only hashes') { expect(@info.find { |k, v| !v.is_a? Hash }).to be nil }
  end

  context '#model_search method result (gsmarena "apple iphone" stub)' do
    before :each do
      @model_list = Parser.model_search('apple iphone')
      @model_names = @model_list.map(&:name)
    end

    it('is array') { expect(@model_list).to be_a Array }
    it('contains 17 elements') { expect(@model_list.size).to eq 17 }
    it('contains "Apple iPhone 5C"') { expect(@model_names).to include 'Apple iPhone 5c' }

    it 'sets gsmarena_url' do
      iphone_6s_plus = @model_list.find { |m| m.name == 'Apple iPhone 6s Plus' }
      expect(iphone_6s_plus.gsmarena_url).to eq 'http://www.gsmarena.com/apple_iphone_6s_plus-7243.php'
    end
  end
end
