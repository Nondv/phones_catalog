# This file is copied to spec/ when you run 'rails generate rspec:install'
ENV['RAILS_ENV'] ||= 'test'
require File.expand_path('../../config/environment', __FILE__)
# Prevent database truncation if the environment is production
abort("The Rails environment is running in production mode!") if Rails.env.production?
require 'spec_helper'
require 'rspec/rails'
require 'webmock/rspec'
# Add additional requires below this line. Rails is not loaded until this point!

# Requires supporting ruby files with custom matchers and macros, etc, in
# spec/support/ and its subdirectories. Files matching `spec/**/*_spec.rb` are
# run as spec files by default. This means that files in spec/support that end
# in _spec.rb will both be required and run as specs, causing the specs to be
# run twice. It is recommended that you do not name files matching this glob to
# end with _spec.rb. You can configure this pattern with the --pattern
# option on the command line or in ~/.rspec, .rspec or `.rspec-local`.
#
# The following line is provided for convenience purposes. It has the downside
# of increasing the boot-up time by auto-requiring all files in the support
# directory. Alternatively, in the individual `*_spec.rb` files, manually
# require only the support files necessary.
#
# Dir[Rails.root.join('spec/support/**/*.rb')].each { |f| require f }

RSpec.configure do |config|
  # RSpec Rails can automatically mix in different behaviours to your tests
  # based on their file location, for example enabling you to call `get` and
  # `post` in specs under `spec/controllers`.
  #
  # You can disable this behaviour by removing the line below, and instead
  # explicitly tag your specs with their type, e.g.:
  #
  #     RSpec.describe UsersController, :type => :controller do
  #       # ...
  #     end
  #
  # The different available types are documented in the features, such as in
  # https://relishapp.com/rspec/rspec-rails/docs
  config.infer_spec_type_from_file_location!

  # Filter lines from Rails gems in backtraces.
  config.filter_rails_from_backtrace!
  # arbitrary gems may also be filtered via:
  # config.filter_gems_from_backtrace("gem name")

  request_stub_body = proc do |name|
    file_path = File.expand_path("spec/request_stubs/#{name}.txt", Rails.root)
    raise "request stub not found: #{file_path}" unless File.exist? file_path

    File.read file_path
  end

  config.before do
    stub_request(:get, 'http://www.gsmarena.com/makers.php3')
      .to_return(body: request_stub_body[:gsmarena_catalog])

    stub_request(:get, 'http://www.gsmarena.com/apple-phones-48.php')
      .to_return(body: request_stub_body[:gsmarena_apple_models])

    stub_request(:get, 'http://www.gsmarena.com/apple_iphone_6s_plus-7243.php')
      .to_return(body: request_stub_body[:gsmarena_iphone_6s_plus])

    stub_request(:get, 'http://www.gsmarena.com/results.php3?sName=apple+iphone&sQuickSearch=yes')
      .to_return(body: request_stub_body[:gsmarena_find_apple_iphone])
  end
end
