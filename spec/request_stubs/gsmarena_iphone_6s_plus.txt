
<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US">
<head>
<title>Apple iPhone 6s Plus - Full phone specifications</title>
<link rel="stylesheet" href="http://st.gsmarena.com/vv/assets8/css/gsmarena.css?v=3">
<!--[if lt IE 9]>
<script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE9.js"></script>
<![endif]-->




<link rel="stylesheet" href="http://st.gsmarena.com/vv/assets8/css/specs.css?v=2">
<link rel="stylesheet" href="http://st.gsmarena.com/vv/assets8/css/comments-2.css">

<meta http-equiv=Content-Type content="text/html; charset=iso-8859-1">
<meta name=Description content="Apple iPhone 6s Plus smartphone. Announced 2015, September. Features 3G, 5.5&Prime; LED-backlit IPS LCD display, 12 MP camera, Wi-Fi, GPS, Bluetooth.">
<meta name=keywords content="Apple iPhone 6s Plus,Apple,iPhone 6s Plus,GSM,mobile,phone,cellphone,information,info,specs,specification,opinion,review">
<link rel="shortcut icon" href="i/favicon.ico" />
<link rel="canonical" href="http://www.gsmarena.com/apple_iphone_6s_plus-7243.php">


</head>
<body>

<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>

<script type="text/javascript" src="http://st.gsmarena.com/vv/assets8/js/misc.js?v=2"></script>
<script type="text/javascript" src="http://st.gsmarena.com/vv/assets8/js/autocomplete.js?ver=3"></script>
<script type="text/javascript" language="javascript">
AUTOCOMPLETE_LIST_URL = "/quicksearch-8089.jpg";
$gsm.addEventListener(document, "DOMContentLoaded", function()
{
    new Autocomplete( "topsearch-text", "topsearch", true );
}
)
</script>
<script language='JavaScript' type='text/javascript'>
function phpads_deliverActiveX(content)
{
	document.write(content);
}
</script>
<header id="header" class="row">
<div class="wrapper clearfix">
<div class="top-bar clearfix">
<!-- HAMBURGER MENU -->
<button type="button" role="button" aria-label="Toggle Navigation" class="lines-button minus focused">
<span class="lines"></span>
</button>
<!-- /HAMBURGER MENU -->



<!-- LOGO -->
<div id="logo">
<a href="/">

<object type="image/svg+xml" data="http://st.gsmarena.com/vv/assets8/i/logo.svg"><img src="http://st.gsmarena.com/vv/assets8/i/logo-fallback.gif" alt="GSMArena.com"></object>
<span>GSMArena.com</span></a>
</div>



<div id="nav" role="main">
<form action="results.php3" method="get" id="topsearch">
    <input type="hidden" name="sQuickSearch" value="yes">
    <input type="text" placeholder="Search" tabindex="201" accesskey="s" id="topsearch-text" name="sName" autocomplete="off" />
    <span id="quick-search-button">
      <input type="submit" value="Go" />
      <i class="head-icon icomoon-liga icon-search-left"></i>
    </span>


</form>
</div>


<div id="social-connect">
<a href="tipus.php3" class="tip-icon">
  <i class="head-icon icon-tip-us icomoon-liga"></i><br><span class="icon-count">Tip us</span>
</a>
<a href="https://www.facebook.com/GSMArenacom-189627474421/" class="fb-icon" target="_blank">
  <i class="head-icon icon-soc-fb2 icomoon-liga"></i><br><span class="icon-count">848k</span>
</a>
<a href="http://twitter.com/gsmarena_com" class="tw-icon" target="_blank">
  <i class="head-icon icon-soc-twitter2 icomoon-liga"></i><br><span class="icon-count">113k</span>
</a>

<a href="https://plus.google.com/116083888509266864115?prsrc=3" class="gp-icon" target="_blank">
  <i class="head-icon icon-soc-gplus icomoon-liga"></i>
  <span class="icon-count">86k</span>
</a>
<a href="http://youtube.com/user/gsmarena07" class="yt-icon" target="_blank">
  <i class="head-icon icon-soc-youtube icomoon-liga"></i><br><span class="icon-count">164k</span>
</a>
<a href="rss-news-reviews.php3" class="rss-icon">
  <i class="head-icon icon-soc-rss2 icomoon-liga"></i><br><span class="icon-count">RSS</span>
</a>




	<a href="#" onclick="return false;" class="login-icon" id="login-active">
	  <i class="head-icon icon-login"></i><br><span class="icon-count" style="right:4px;">Log in</span>
	</a>

	<span class="tooltip" id="login-popup">
	   <p>
	     Login with<br />
	     <a href="login-facebook.php3?rdr=MH5vb3N6QHZvd3BxekApbEBvc2psMigtKywxb3dv"><i class="head-icon icon-soc-fb2"></i>Facebook</a>
	     <a class="g-login" href="login-google.php3?rdr=MH5vb3N6QHZvd3BxekApbEBvc2psMigtKywxb3dv"><i class="head-icon icon-soc-gplus"></i>Google</a>
	   </p>
	</span>
 <a href="nickname.php3?sSource=MH5vb3N6QHZvd3BxekApbEBvc2psMigtKywxb3dv" class="signup-icon no-margin-right"><i class="head-icon icon-user-plus"></i><span class="icon-count">Sign up</span></a>
              </div>
           </div>
<ul id="menu" class="main-menu-list">
<li><a href="/">Home</a></li>
	<li><a href="news.php3">News</a></li>
	<li><a href="reviews.php3">Reviews</a></li>
	<li><a href="blog.php3">Blog</a></li>
	<li><a href="compare.php3">Compare</a></li>
	<li><a href="tools.php3">Tools</a></li>
	<li><a href="glossary.php3">Glossary</a></li>
	<li><a href="network-bands.php3">Coverage</a></li>
	<li><a href="faq.php3">FAQ</a></li>
	<li><a href="contact.php3">Contact</a></li>

      </ul>
      <!-- SOCIAL CONNECT -->
    </div>


</header> <!--- HEADER END -->



<script type="text/javascript" src="http://st.gsmarena.com/vv/assets8/js/specs.js?v=2"></script>
<script type="text/javascript" language="javascript">
HISTORY_ITEM_ID = "7243";
HISTORY_ITEM_NAME = "Apple iPhone 6s Plus";
HISTORY_ITEM_URL = "apple_iphone_6s_plus-7243.php";
HISTORY_ITEM_IMAGE = "http://cdn2.gsmarena.com/vv/bigpic/apple-iphone-6s-plus.jpg";

</script>
<div id="wrapper" class="l-container">
<div id="outer" class="row">


<div id="subHeader" class="col">
<div id="topPromo">
		<div class="alt-display is-review" style="background-image: url(http://cdn.gsmarena.com/imgroot/reviews/16/samsung-galaxy-s7/-347x151/gsmarena_101.jpg)">
		  <a class="table-cell reviews-xl-snazzy" href="samsung_galaxy_s7_time_saver-review-1412.php">
			  <div class="module-review-xl-title">

			    	<span>Time-saver edition</span><br>
			    	<strong>Samsung Galaxy S7 review</strong>


		 	  </div>
		 </a>
	  </div>
</div><div id="topAdv" class="l-box">
<script language='JavaScript' type='text/javascript'>
<!--
   if (!document.phpAds_used) document.phpAds_used = ',';
   phpAds_random = new String (Math.random()); phpAds_random = phpAds_random.substring(2,11);

   document.write ("<" + "script language='JavaScript' type='text/javascript' src='");
   document.write ("http://a.gsmarena.com/adjs.php?n=" + phpAds_random);
   document.write ("&amp;what=zone:5&amp;target=_blank");
   document.write ("&amp;exclude=" + document.phpAds_used);
   if (document.referrer)
      document.write ("&amp;referer=" + escape(document.referrer));
   document.write ("'><" + "/script>");
//-->
</script><noscript><a href='http://a.gsmarena.com/adclick.php?n=afad88d6' target='_blank'><img src='http://a.gsmarena.com/adview.php?what=zone:5&amp;n=afad88d6' border='0' alt=''></a></noscript>
</div>
</div>


<div id="body">

<div class="main main-review right l-box col">

<div class="review-header hreview">
    <div class="article-info">
<style type="text/css">
    .review-header::after {
	background-image:url( http://cdn2.gsmarena.com/vv/bigpic/apple-iphone-6s-plus.jpg );
	background-size: 413%;
	background-position: -1674px -2577px;
    }
</style>


        <div class="article-info-line page-specs light border-bottom">
            <div class="blur review-background"></div>
            <h1 class="specs-phone-name-title">Apple iPhone 6s Plus</h1>



<script>
	var sURLSocialE = "http%3A%2F%2Fwww.gsmarena.com%2Fapple_iphone_6s_plus-7243.php";
</script>


<ul class="social-share sharrre">
<li class="help help-social">
  <a class="box btnFb" title="Facebook" id="facebook" href="http://www.facebook.com/sharer.php?u=http%3A%2F%2Fwww.gsmarena.com%2Fapple_iphone_6s_plus-7243.php" target="_blank" onclick="window.open('http://www.facebook.com/sharer.php?u=http%3A%2F%2Fwww.gsmarena.com%2Fapple_iphone_6s_plus-7243.php','fbookshare','width=500,height=400,left='+(screen.availWidth/2-225)+',top='+(screen.availHeight/2-150)+'');return false;">

    <div class="share">
      <span class="count fbCount" href="#">-</span>
      <i class="head-icon icon-soc-fb2"></i>
    </div>
  </a>
</li>
<li>
  <a class="box btnTw" title="Twitter" id="twitter" href="https://twitter.com/intent/tweet?text=Apple+iPhone+6s+Plus&url=http%3A%2F%2Fwww.gsmarena.com%2Fapple_iphone_6s_plus-7243.php" target="_blank" onclick="window.open('https://twitter.com/intent/tweet?text=Apple+iPhone+6s+Plus&url=http%3A%2F%2Fwww.gsmarena.com%2Fapple_iphone_6s_plus-7243.php','twitshare','width=500,height=400,left='+(screen.availWidth/2-225)+',top='+(screen.availHeight/2-150)+'');return false;">
    <div class="share">
      <span class="count twCount" href="#">-</span>
      <i class="head-icon icon-soc-twitter2"></i></div>
  </a>
</li>
<li>
  <a class="box btnGo" title="Google+" id="googleplus" href="https://plusone.google.com/_/+1/confirm?hl=en&url=http%3A%2F%2Fwww.gsmarena.com%2Fapple_iphone_6s_plus-7243.php" target="_blank" onclick="window.open('https://plus.google.com/share?url=http%3A%2F%2Fwww.gsmarena.com%2Fapple_iphone_6s_plus-7243.php','gplusshare','width=500,height=400,left='+(screen.availWidth/2-225)+',top='+(screen.availHeight/2-150)+'');return false;">

    <div class="share">
      <span class="count goCount" href="#">-</span>
      <i class="head-icon icon-soc-gplus"></i>
    </div>
  </a>
</li>
</ul>


        </div>






<div class="center-stage light nobg specs-accent">
  <div class="specs-photo-main">
<a href=apple_iphone_6s_plus-pictures-7243.php><img alt="Apple iPhone 6s Plus
MORE PICTURES" src=http://cdn2.gsmarena.com/vv/bigpic/apple-iphone-6s-plus.jpg></a>
  </div>
  <ul class="specs-spotlight-features" style="overflow:hidden;">
    <li class="specs-brief pattern">
      <span class="specs-brief-accent"><i class="head-icon icon-launched"></i>Released 2015, September</span><br>
      <span class="specs-brief-accent"><i class="head-icon icon-mobile2"></i>192g, 7.3mm thickness</span><br>
      <span class="specs-brief-accent"><i class="head-icon icon-os"></i>iOS 9, up to iOS 9.3.2</span><br>
      <span class="specs-brief-accent"><i class="head-icon icon-sd-card-0"></i>16/64/128GB storage, no card slot</span>

      <span class="help help-quickfacts"></span>
    </li>

    <li class="light pattern help help-popularity">

      <strong class="accent"> <i class="head-icon icon-popularity"></i>19%</strong>
      <span>8,489,104 hits</span></li>
    <li class="light pattern help help-fans">
      <a class="specs-fans" href="">
        <strong class="accent"><i class="head-icon icon-fans"></i>572</strong>
        <span>Become a fan</span>
      </a>
    </li>
    <li class="help accented help-display">
      <i class="head-icon icon-touch-1"></i>
      <strong class="accent">5.5"</strong>1080x1920 pixels    </li>
    <li class="help accented help-camera">
      <i class="head-icon icon-camera-1"></i>
      <strong class="accent accent-camera">12<span>MP</span>
      </strong> 2160p</li>
    <li class="help accented help-expansion">
      <i class="head-icon icon-cpu"></i>
      <strong class="accent accent-expansion">2<span>GB RAM</span></strong>
      Apple A9</li>


    <li class="help accented help-battery">
    <i class="head-icon icon-battery-1"></i>

    <strong class="accent accent-battery">2750<span>mAh</span></strong>Li-Po</li>

  </ul>
</div>

<div class="article-info-line page-specs light">

<ul class="article-info-meta">

<li class="article-info-meta-link article-info-meta-link-review light large help help-review"><a href="apple_iphone_6s_plus-review-1316.php"><i class="head-icon icon-review"></i>Review</a></li><li class="article-info-meta-link light"><a href=apple_iphone_6s_plus-3d-spin-7243.php><i class="head-icon icon-360-view"></i>360&deg; view</a></li>

<li class="article-info-meta-link light"><a href=apple_iphone_6s_plus-pictures-7243.php><i class="head-icon icon-pictures"></i>Pictures</a></li>
<li class="article-info-meta-link light"><a href=compare.php3?idPhone1=7243><i class="head-icon icon-mobile-phone231"></i>Compare</a></li>
<li class="article-info-meta-link light" title="Apple iPhone 6s Plus user reviews and opinions"><a href="apple_iphone_6s_plus-reviews-7243.php"><i class="head-icon icon-comment-count"></i>Opinions</a></li>


<br style="clear:both;" />
</ul>
</div>

</div>
</div>


<script language="JavaScript">
<!--
function helpW( strURL )
{
window.open( 'help.php3?term=' + strURL, '_blank', 'toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=no,width=420,height=390' );
}
//-->
</script>

<div id="specs-list">

<style type="text/css"> .tr-toggle {  display:none; }  </style>


<table cellspacing="0" style="height: 24px;">
<tr class="tr-hover">
<th rowspan="15" scope="row">Network</th>
<td class="ttl"><a href="network-bands.php3">Technology</a></td>
<td class="nfo"><a href="#" class="link-network-detail collapse">GSM / CDMA / HSPA / EVDO / LTE</a></td>
</tr>
<tr class="tr-toggle">
<td class="ttl"><a href="network-bands.php3">2G bands</a></td>
<td class="nfo">GSM 850 / 900 / 1800 / 1900 </td>
</tr><tr class="tr-toggle">
<td class="ttl">&nbsp;</td>
<td class="nfo">CDMA 800 / 1700 / 1900 / 2100 </td>
</tr>
<tr class="tr-toggle">
<td class="ttl"><a href="network-bands.php3">3G bands</a></td>
<td class="nfo">HSDPA 850 / 900 / 1700 / 1900 / 2100 </td>
</tr>
<tr class="tr-toggle">
<td class="ttl">&nbsp;</td>
<td class="nfo">CDMA2000 1xEV-DO </td>
</tr>
<tr class="tr-toggle">
<td class="ttl"><a href="network-bands.php3">4G bands</a></td>
<td class="nfo">LTE band 1(2100), 2(1900), 3(1800), 4(1700/2100), 5(850), 7(2600), 8(900), 12(700), 13(700), 17(700), 18(800), 19(800), 20(800), 25(1900), 26(850), 28(700), 29(700), 30(2300) - A1633</td>
</tr>
<tr class="tr-toggle">
<td class="ttl">&nbsp;</td>
<td class="nfo">LTE band 1(2100), 2(1900), 3(1800), 4(1700/2100), 5(850), 7(2600), 8(900), 12(700), 13(700), 17(700), 18(800), 19(800), 20(800), 25(1900), 26(850), 28(700), 29(700), 30(2300), 38(2600), 39(1900), 40(2300), 41(2500) - A1634</td>
</tr>
<tr class="tr-toggle">
<td class="ttl"><a href="glossary.php3?term=3g">Speed</a></td>
<td class="nfo">HSPA 42.2/5.76 Mbps, LTE Cat6 300/50 Mbps, EV-DO Rev.A 3.1 Mbps</td>
</tr>

<tr class="tr-toggle">
<td class="ttl"><a href="glossary.php3?term=gprs">GPRS</a></td>
<td class="nfo">Yes</td>
</tr>
<tr class="tr-toggle">
<td class="ttl"><a href="glossary.php3?term=edge">EDGE</a></td>
<td class="nfo">Yes</td>
</tr>
</table>


<table cellspacing="0">
<tr>
<th rowspan="2" scope="row">Launch</th>
<td class="ttl"><a href=# onClick="helpW('h_year.htm');">Announced</a></td>
<td class="nfo">2015, September</td>
</tr>
<tr>
<td class="ttl"><a href=# onClick="helpW('h_status.htm');">Status</a></td>
<td class="nfo">Available. Released 2015, September</td>
</tr>
</table>


<table cellspacing="0">
<tr>
<th rowspan="6" scope="row">Body</th>
<td class="ttl"><a href=# onClick="helpW('h_dimens.htm');">Dimensions</a></td>
<td class="nfo">158.2 x 77.9 x 7.3 mm (6.23 x 3.07 x 0.29 in)</td>
</tr><tr>
<td class="ttl"><a href=# onClick="helpW('h_weight.htm');">Weight</a></td>
<td class="nfo">192 g (6.77 oz)</td>
</tr>
<tr>
<td class="ttl"><a href="glossary.php3?term=sim">SIM</a></td>
<td class="nfo">Nano-SIM</td>
</tr>
<tr><td class="ttl">&nbsp;</td><td class="nfo">- Apple Pay (Visa, MasterCard, AMEX certified)</td></tr>

</table>


<table cellspacing="0">
<tr>
<th rowspan="6" scope="row">Display</th>
<td class="ttl"><a href="glossary.php3?term=display-type">Type</a></td>
<td class="nfo">LED-backlit IPS LCD, capacitive touchscreen, 16M colors</td>
</tr>
<tr>
<td class="ttl"><a href=# onClick="helpW('h_dsize.htm');">Size</a></td>
<td class="nfo">5.5 inches (~67.7% screen-to-body ratio)</td>
</tr>
<tr>
<td class="ttl"><a href=# onClick="helpW('h_dres.htm');">Resolution</a></td>
<td class="nfo">1080 x 1920 pixels (~401 ppi pixel density)</td>
</tr>
<tr>
<td class="ttl"><a href="glossary.php3?term=multitouch">Multitouch</a></td>
<td class="nfo">Yes</td>
</tr>
<tr>
<td class="ttl"><a href="glossary.php3?term=screen-protection">Protection</a></td>
<td class="nfo">Ion-strengthened glass, oleophobic coating</td>
</tr>
<tr><td class="ttl">&nbsp;</td><td class="nfo">- 3D Touch display<br />
- Display Zoom</td></tr>

</table>


<table cellspacing="0">
<tr>
<th rowspan="4" scope="row">Platform</th>
<td class="ttl"><a href="glossary.php3?term=os">OS</a></td>
<td class="nfo">iOS 9, upgradable to iOS 9.3.2, planned upgrade to iOS 10</td>
</tr>
<tr><td class="ttl"><a href="glossary.php3?term=chipset">Chipset</a></td>
<td class="nfo">Apple A9</td>
</tr>
<tr><td class="ttl"><a href="glossary.php3?term=cpu">CPU</a></td>
<td class="nfo">Dual-core 1.84 GHz Twister</td>
</tr>
<tr><td class="ttl"><a href="glossary.php3?term=gpu">GPU</a></td>
<td class="nfo">PowerVR GT7600 (six-core graphics)</td>
</tr>
</table>


<table cellspacing="0">
<tr>
<th rowspan="5" scope="row">Memory</th>
<td class="ttl"><a href="glossary.php3?term=memory-card-slot">Card slot</a></td>


<td class="nfo">No</td></tr>



<tr>
<td class="ttl"><a href="glossary.php3?term=dynamic-memory">Internal</a></td>
<td class="nfo">16/64/128 GB, 2 GB RAM</td>
</tr>



</td>
</tr>
</table>


<table cellspacing="0">
<tr>
<th rowspan="4" scope="row">Camera</th>
<td class="ttl"><a href="glossary.php3?term=camera">Primary</a></td>
<td class="nfo">12 MP, f/2.2, 29mm, phase detection autofocus, OIS, dual-LED (dual tone) flash, <a href="piccmp.php3?idType=1&idPhone1=7243&nSuggest=1">check quality</a></td>
</tr>
<tr>
<td class="ttl"><a href="glossary.php3?term=camera">Features</a></td>
<td class="nfo">1/3" sensor size, 1.22 �m pixel size, geo-tagging, simultaneous 4K video and 8MP image recording, touch focus, face/smile detection, HDR (photo/panorama)</td>
</tr>
<tr>
<td class="ttl"><a href="glossary.php3?term=camera">Video</a></td>
<td class="nfo">2160p@30fps, 1080p@60fps, 1080p@120fps, 720p@240fps, <a href="vidcmp.php3?idType=3&idPhone1=7243&nSuggest=1">check quality</a></td>
</tr>
<tr>
<td class="ttl"><a href="glossary.php3?term=video-call">Secondary</a></td>
<td class="nfo">5 MP, f/2.2, 31mm, 1080p@30fps, 720p@240fps, face detection, HDR, panorama</td>
</tr>
</table>


<table cellspacing="0">
<tr>
<th rowspan="4" scope="row">Sound</th>
<td class="ttl"><a href="glossary.php3?term=call-alerts">Alert types</a></td>
<td class="nfo">Vibration, proprietary ringtones</td>
	</tr>

<tr>
<td class="ttl"><a href="glossary.php3?term=loudspeaker">Loudspeaker</a> </td>
<td class="nfo">Yes</td>
</tr>

<tr>
<td class="ttl"><a href="glossary.php3?term=audio-jack">3.5mm jack</a> </td>
<td class="nfo">Yes</td>
</tr>


<tr><td class="ttl">&nbsp;</td><td class="nfo">- 16-bit/44.1kHz audio<br />
- Active noise cancellation with dedicated mic</td></tr>

</table>


<table cellspacing="0">
<tr>
<th rowspan="9" scope="row">Comms</th>
<td class="ttl"><a href="glossary.php3?term=wi-fi">WLAN</a></td>
<td class="nfo">Wi-Fi 802.11 a/b/g/n/ac, dual-band, hotspot</td>
</tr>
<tr>
<td class="ttl"><a href="glossary.php3?term=bluetooth">Bluetooth</a></td>
<td class="nfo">v4.2, A2DP, LE</td>
</tr>
<tr>
<td class="ttl"><a href="glossary.php3?term=gps">GPS</a></td>
<td class="nfo">Yes, with A-GPS, GLONASS</td>
</tr>
<tr>
<td class="ttl"><a href="glossary.php3?term=nfc">NFC</a></td>
<td class="nfo">Yes (Apple Pay only)</td>
</tr>


<tr>
<td class="ttl"><a href="glossary.php3?term=fm-radio">Radio</a></td>
<td class="nfo">No</td>
</tr>

<tr>
<td class="ttl"><a href="glossary.php3?term=usb">USB</a></td>
<td class="nfo">v2.0, reversible connector</td>
</tr>
</table>


<table cellspacing="0">
<tr>
<th rowspan="12" scope="row">Features</th>
<td class="ttl"><a href="glossary.php3?term=sensors">Sensors</a></td>
<td class="nfo">Fingerprint, accelerometer, gyro, proximity, compass, barometer</td>
</tr><tr>
<td class="ttl"><a href="glossary.php3?term=messaging">Messaging</a></td>
<td class="nfo">iMessage, SMS (threaded view), MMS, Email, Push Email</td>
</tr><tr>
<td class="ttl"><a href="glossary.php3?term=browser">Browser</a></td>
<td class="nfo">HTML5 (Safari)</td>
</tr>


<tr>
<td class="ttl"><a href="glossary.php3?term=java">Java</a></td>
<td class="nfo">No</td>
</tr>
<tr><td class="ttl">&nbsp;</td><td class="nfo">- Siri natural language commands and dictation<br />
- iCloud cloud service<br />
- MP3/WAV/AAX+/AIFF/Apple Lossless player<br />
- MP4/H.264 player<br />
- Audio/video/photo editor<br />
- Document editor</td></tr>

</table>


<table cellspacing="0">
<tr>
<th rowspan="7" scope="row">Battery</th>
<td class="ttl">&nbsp;</td>
<td class="nfo">Non-removable Li-Po 2750 mAh battery (10.45 Wh)</td>
</tr>
<tr>
<td class="ttl"><a href="glossary.php3?term=stand-by-time">Stand-by</a></td>
<td class="nfo">Up to 384 h (3G)</td>
</tr>

<tr>
<td class="ttl"><a href="glossary.php3?term=talk-time">Talk time</a></td>
<td class="nfo">Up to 24 h (3G)</td>
</tr>

<tr>
<td class="ttl"><a href="glossary.php3?term=music-playback-time">Music play</a></td>
<td class="nfo">Up to 80 h</td>
</tr>
</table>


<table cellspacing="0">
<tr>
<th rowspan="5" scope="row">Misc</th>
<td class="ttl"><a href=# onClick="helpW('h_colors.htm');">Colors</a></td>
<td class="nfo">Space Gray, Silver, Gold, Rose Gold</td>
</tr>
<tr>
<td class="ttl"><a href="glossary.php3?term=sar">SAR US</a></td>
<td class="nfo">
1.12 W/kg (head) &nbsp; &nbsp; 1.14 W/kg (body) &nbsp; &nbsp; </td>
</tr>
<tr>
<td class="ttl"><a href="glossary.php3?term=sar">SAR EU</a></td>
<td class="nfo">
0.93 W/kg (head) &nbsp; &nbsp; 0.98 W/kg (body) &nbsp; &nbsp; </td>
</tr>


<tr>
<td class="ttl"><a href=# onClick="helpW('h_price.htm');">Price group</a></td>
<td class="nfo"><a href="#">9/10</a> <span class="price">(About 750 EUR)</span></td>
</tr>
</table>


<table cellspacing="0">
<tr>
<th rowspan="6" scope="row">Tests</th>
<td class="ttl"><a href="glossary.php3?term=benchmarking">Performance</a></td>
<td class="nfo">
<a class="noUnd" href="benchmark-test.php3?idPhone=7243#show">Basemark OS II 2.0: 2261</a></td>
</tr><tr>

<td class="ttl"><a href="gsmarena_lab_tests-review-751p2.php">Display</a></td>
<td class="nfo">
<a class="noUnd" href="http://www.gsmarena.com/apple_iphone_6s_plus-review-1316p3.php">Contrast ratio: 1382:1 (nominal), 3.530 (sunlight)</a></td>
</tr><tr>

<td class="ttl"><a href="gsmarena_lab_tests-review-751p5.php">Camera</a></td>
<td class="nfo">
<a class="noUnd" href="piccmp.php3?idType=1&idPhone1=7243&nSuggest=1">Photo</a> / <a class="noUnd" href="vidcmp.php3?idType=3&idPhone1=7243&nSuggest=1">Video</a></td>
</tr><tr>

<td class="ttl"><a href="gsmarena_lab_tests-review-751p3.php">Loudspeaker</a></td>
<td class="nfo">
<a class="noUnd" href="http://www.gsmarena.com/apple_iphone_6s_plus-review-1316p7.php">Voice 65dB / Noise 65dB / Ring 64dB</a>

</td>
</tr><tr>

<td class="ttl"><a href="gsmarena_lab_tests-review-751p4.php">Audio quality</a></td>
<td class="nfo">
<a class="noUnd" href="http://www.gsmarena.com/apple_iphone_6s_plus-review-1316p8.php#aq">Noise -93.4dB / Crosstalk -71.1dB</a></td>
</tr><tr>


<td class="ttl"><a href="gsmarena_lab_tests-review-751p6.php">Battery life</a></td>
<td class="nfo">
<div style="position:relative;">
<a href="#" onclick="showBatteryPopup(event, 7243); ">Endurance rating 85h</a><div class="popover top" id="battery-popover" style="display: none;"></div>
</div>
</td>
</tr><tr>

</tr></table>



</div>




<p class="note"><strong>Disclaimer.</strong> We can not guarantee that the information on this page is 100% correct. <a href=# onClick="helpW('legal.htm');">Read more</a></p>


<div class="article-info-line page-specs light">

<ul class="article-info-meta">

<li class="article-info-meta-link article-info-meta-link-review light large help help-review"><a href="apple_iphone_6s_plus-review-1316.php"><i class="head-icon icon-review"></i>Review</a></li><li class="article-info-meta-link light"><a href=apple_iphone_6s_plus-3d-spin-7243.php><i class="head-icon icon-360-view"></i>360&deg; view</a></li>

<li class="article-info-meta-link light"><a href=apple_iphone_6s_plus-pictures-7243.php><i class="head-icon icon-pictures"></i>Pictures</a></li>
<li class="article-info-meta-link light"><a href=compare.php3?idPhone1=7243><i class="head-icon icon-mobile-phone231"></i>Compare</a></li>
<li class="article-info-meta-link light" title="Apple iPhone 6s Plus user reviews and opinions"><a href="apple_iphone_6s_plus-reviews-7243.php"><i class="head-icon icon-comment-count"></i>Opinions</a></li>


<br style="clear:both;" />
</ul>
</div>



<div id="user-comments" class="box s3 nobg">
<h2><a href="apple_iphone_6s_plus-reviews-7243.php">Apple iPhone 6s Plus - user opinions and reviews</a></h2>

<div class="user-thread">
<div class="uavatar"><i class="head-icon icon-user"></i></div>
<ul class="uinfo2">
<li class="uname2">duh</li>
<li class="ulocation">
<i class="head-icon icon-location"></i>
<span title="Encoded location">K3ee</span></li>
<li class="upost"><time>17 hours ago</time></li>
</ul>
<p class="uopin">hahahahaha is that a real question? the answer is that your face will disappear if you get it close to you</p>

<ul class="uinfo">
<li class="ureply">
<span title="Reply to this post">
<a href="postopinion.php3?idPhone=7243&idOpinion=5257199">Reply</a></span>
</li>


</ul>




</div>

<div class="user-thread">
<div class="uavatar"><i class="head-icon icon-user"></i></div>
<ul class="uinfo2">
<li class="uname2">kishan</li>
<li class="ulocation">
<i class="head-icon icon-location"></i>
<span title="Encoded location">rJe5</span></li>
<li class="upost"><time>09 Jul 2016</time></li>
</ul>
<p class="uopin">How many time charged 100%battry in i phone 6s pluse?
Pls reply</p>

<ul class="uinfo">
<li class="ureply">
<span title="Reply to this post">
<a href="postopinion.php3?idPhone=7243&idOpinion=5255419">Reply</a></span>
</li>


</ul>




</div>

<div class="user-thread">
<div class="uavatar"><i class="head-icon icon-user"></i></div>
<ul class="uinfo2">
<li class="uname2">ron</li>
<li class="ulocation">
<i class="head-icon icon-location"></i>
<span title="Encoded location">a4$S</span></li>
<li class="upost"><time>09 Jul 2016</time></li>
</ul>
<p class="uopin">there is a new problem with iphone 6 plus
ghost touch or phantom touch and no solution.</p>

<ul class="uinfo">
<li class="ureply">
<span title="Reply to this post">
<a href="postopinion.php3?idPhone=7243&idOpinion=5254955">Reply</a></span>
</li>


</ul>




</div>

<div class="sub-footer">
<div class="button-links">
<ul>

<li><a class="button" href="apple_iphone_6s_plus-reviews-7243.php">Read all opinions</a></li>
<li><a class="button" href="postopinion.php3?idPhone=7243">Post your opinion</a></li>
</ul>

</div>
<div id="opinions-total">Total user opinions: <b>1470</b></div>
<br class="clear" />

						</div>



</div>
</div>

<aside class="sidebar col left">

<div class="brandmenu-v2 light l-box clearfix">
<p class="pad">
<a href="search.php3" class="pad-single pad-finder">
<i class="head-icon icon-search-right"></i>
<span>Phone finder</span></a>
</p>
<ul>
<li><a href="samsung-phones-9.php">Samsung</a></li><li><a href="apple-phones-48.php">Apple</a></li><li><a href="microsoft-phones-64.php">Microsoft</a></li><li><a href="nokia-phones-1.php">Nokia</a></li><li><a href="sony-phones-7.php">Sony</a></li><li><a href="lg-phones-20.php">LG</a></li><li><a href="htc-phones-45.php">HTC</a></li><li><a href="motorola-phones-4.php">Motorola</a></li><li><a href="huawei-phones-58.php">Huawei</a></li><li><a href="lenovo-phones-73.php">Lenovo</a></li><li><a href="xiaomi-phones-80.php">Xiaomi</a></li><li><a href="acer-phones-59.php">Acer</a></li><li><a href="asus-phones-46.php">Asus</a></li><li><a href="oppo-phones-82.php">Oppo</a></li><li><a href="oneplus-phones-95.php">OnePlus</a></li><li><a href="meizu-phones-74.php">Meizu</a></li><li><a href="blackberry-phones-36.php">BlackBerry</a></li><li><a href="alcatel-phones-5.php">Alcatel</a></li><li><a href="zte-phones-62.php">ZTE</a></li><li><a href="toshiba-phones-44.php">Toshiba</a></li><li><a href="vodafone-phones-53.php">Vodafone</a></li><li><a href="gigabyte-phones-47.php">Gigabyte</a></li><li><a href="pantech-phones-32.php">Pantech</a></li><li><a href="xolo-phones-85.php">XOLO</a></li><li><a href="lava-phones-94.php">Lava</a></li><li><a href="micromax-phones-66.php">Micromax</a></li><li><a href="blu-phones-67.php">BLU</a></li><li><a href="gionee-phones-92.php">Gionee</a></li><li><a href="vivo-phones-98.php">vivo</a></li><li><a href="yu-phones-100.php">YU</a></li><li><a href="verykool-phones-70.php">verykool</a></li><li><a href="maxwest-phones-87.php">Maxwest</a></li><li><a href="niu-phones-79.php">NIU</a></li><li><a href="yezz-phones-78.php">Yezz</a></li><li><a href="parla-phones-81.php">Parla</a></li><li><a href="plum-phones-72.php">Plum</a></li></ul>

<p class="pad">
<a href="makers.php3" class="pad-multiple pad-allbrands">
  <i class="head-icon icon-mobile-phone231"></i>
  <span>All brands</span>
</a>
<a href="rumored.php3" class="pad-multiple pad-rumormill">
  <i class="head-icon icon-rumored"></i>
  <span>Rumor mill</span>
</a>
</p>
</div>
<div class="adv banner-mpu">

<script language='JavaScript' type='text/javascript'>
<!--
   if (!document.phpAds_used) document.phpAds_used = ',';
   phpAds_random = new String (Math.random()); phpAds_random = phpAds_random.substring(2,11);

   document.write ("<" + "script language='JavaScript' type='text/javascript' src='");
   document.write ("http://a.gsmarena.com/adjs.php?n=" + phpAds_random);
   document.write ("&amp;what=zone:156&amp;target=_blank");
   document.write ("&amp;exclude=" + document.phpAds_used);
   if (document.referrer)
      document.write ("&amp;referer=" + escape(document.referrer));
   document.write ("'><" + "/script>");
//-->
</script><noscript><a href='http://a.gsmarena.com/adclick.php?n=afad88d6' target='_blank'><img src='http://a.gsmarena.com/adview.php?what=zone:5&amp;n=afad88d6' border='0' alt=''></a></noscript>
</div>


<div class="module module-phones module-more-promo">
<h4 class="section-heading">More</h4>
<ul>



<li class="in-the-news-list"><strong><a href="news.php3?idPhoneSearch=7243">In the news</a></strong></li>





</ul>
</div>



<style type="text/css">
.checkprice-list::before {
    background: url('http://cdn2.gsmarena.com/vv/bigpic/apple-iphone-6s-plus.jpg') no-repeat center center;
  }
</style>


<style type="text/css">
	.checkprice-list::before {
	background-size: 413% !important;
	background-position:  -690px -1062px;
	}
</style>

<div class="module module-phones module-checkprice">
<h4 class="section-heading">Check Price</h4>
<ul class="checkprice-list">
<li><a href=http://a.gsmarena.com/adclick.php?bannerid=2434&zoneid=9&source=&dest=https%3A%2F%2Fmelectronicsinc.com%2Fgsm-apple target=_blank><i class="head-icon icon-cart"></i>Melectronics</a></li><li><a href=http://a.gsmarena.com/adclick.php?bannerid=2263&zoneid=9&source=&dest=https%3A%2F%2Fwww.cellpex.com%2Findex.php%3Fadv_id%3Ddeda67ae%26fuseaction%3Dsearch.filter%26ref%3D1%26q%3DApple%2520iPhone%25206s%2520Plus target=_blank><i class="head-icon icon-cart"></i>Cellpex (Wholesale)</a></li></ul>
</div>





<div class="module reviews-xl-snazzy">
<h4 class="section-heading">Apple iPhone 6s Plus reviews</h4>

<a href="apple_iphone_6s_plus-review-1316.php" class="module-reviews-xl-link">
  <img class="module-reviews-xl-thumb" src="http://cdn.gsmarena.com/imgroot/reviews/15/apple-iphone-6s-plus/-347x151/gsmarena_001.jpg" alt="Apple iPhone 6s Plus review: The bigger picture">
  <div class="module-review-xl-title">
    <span>The bigger picture</span><br>
    <strong>Apple iPhone 6s Plus review</strong>
  </div>
</a>
<a href="iphone_6s_plus_vs_s6_edge_plus-review-1324.php" class="module-reviews-xl-link">
  <img class="module-reviews-xl-thumb" src="http://cdn.gsmarena.com/imgroot/reviews/15/iphone-6s-plus-vs-s6-edge-plus/-347x151/thumb2.jpg" alt="Apple iPhone 6s Plus vs. Samsung Galaxy S6 edge+: Double positive">
  <div class="module-review-xl-title">
    <span>Double positive</span><br>
    <strong>Apple iPhone 6s Plus vs. Samsung Galaxy S6 edge+</strong>
  </div>
</a>

</div>

<div class="module">


<script type="text/javascript" src='http://adn.ebay.com/files/js/min/ebay_activeContent-min.js'></script>
<script charset="utf-8" type="text/javascript">
document.write('\x3Cscript type="text/javascript" charset="utf-8" src="http://adn.ebay.com/cb?programId=1&campId=5336676177&toolId=10026&customId=sky&keyword=Apple+iPhone+6s+Plus&minPrice=50&width=300&height=308&font=1&textColor=333366&linkColor=333333&arrowColor=FFAF5E&color1=63769A&color2=[COLORTWO]&format=ImageLink&contentType=TEXT_AND_IMAGE&enableSearch=y&usePopularSearches=n&freeShipping=n&topRatedSeller=n&itemsWithPayPal=n&descriptionSearch=n&showKwCatLink=n&excludeCatId=&excludeKeyword=&catId=3312&disWithin=200&ctx=n&autoscroll=n&flashEnabled=' + isFlashEnabled + '&pageTitle=' + _epn__pageTitle + '&cachebuster=' + (Math.floor(Math.random() * 10000000 )) + '">\x3C/script>' );
</script>

</div>





<div class="module">
<h4 class="section-heading">Video review</h4>
<iframe width="300" height="169" src="https://www.youtube.com/embed/L7yGeafSLrA" frameborder="0" allowfullscreen></iframe>
</div>

<div class="module module-phones module-related">
<h4 class="section-heading"><a href="related.php3?idPhone=7243">Related devices</a></h4>

<ul class="clearfix">
<li><a class="module-phones-link" href="apple_iphone_6s-7242.php"><img src="http://cdn2.gsmarena.com/vv/bigpic/apple-iphone-6s1.jpg" alt="Phone" /><br>Apple iPhone 6s</a></li><li><a class="module-phones-link" href="samsung_galaxy_s7_edge-7945.php"><img src="http://cdn2.gsmarena.com/vv/bigpic/samsung-galaxy-s7-edge-.jpg" alt="Phone" /><br>Samsung Galaxy S7 edge</a></li><li><a class="module-phones-link" href="apple_iphone_6_plus-6665.php"><img src="http://cdn2.gsmarena.com/vv/bigpic/apple-iphone-6-plus2.jpg" alt="Phone" /><br>Apple iPhone 6 Plus</a></li><li><a class="module-phones-link" href="oneplus_3-7995.php"><img src="http://cdn2.gsmarena.com/vv/bigpic/oneplus-3-.jpg" alt="Phone" /><br>OnePlus 3</a></li><li><a class="module-phones-link" href="sony_xperia_z5_premium-7536.php"><img src="http://cdn2.gsmarena.com/vv/bigpic/sony-z5-premium-.jpg" alt="Phone" /><br>Sony Xperia Z5 Premium</a></li><li><a class="module-phones-link" href="samsung_galaxy_note5-7431.php"><img src="http://cdn2.gsmarena.com/vv/bigpic/samsung-galaxy-note5.jpg" alt="Phone" /><br>Samsung Galaxy Note5</a></li>
</ul>
<p class="more"><a class="more-news-link" href="related.php3?idPhone=7243">More related devices</a></p>
</div>


<div class="module module-phones module-related phonelist">
<h4 class="section-heading"><a href=apple-phones-48.php>Popular from Apple</a></h4>

<ul class="clearfix">
<li><a class="module-phones-link" href="apple_iphone_5s-5685.php"><img src="http://cdn2.gsmarena.com/vv/bigpic/apple-iphone-5s-ofic.jpg" alt="Phone" /><br>Apple iPhone 5s</a></li><li><a class="module-phones-link" href="apple_iphone_6s-7242.php"><img src="http://cdn2.gsmarena.com/vv/bigpic/apple-iphone-6s1.jpg" alt="Phone" /><br>Apple iPhone 6s</a></li><li><a class="module-phones-link" href="apple_iphone_6-6378.php"><img src="http://cdn2.gsmarena.com/vv/bigpic/apple-iphone-6-4.jpg" alt="Phone" /><br>Apple iPhone 6</a></li><li><a class="module-phones-link" href="apple_iphone_se-7969.php"><img src="http://cdn2.gsmarena.com/vv/bigpic/apple-iphone-5se-ofic.jpg" alt="Phone" /><br>Apple iPhone SE</a></li><li><a class="module-phones-link" href="apple_iphone_5-4910.php"><img src="http://cdn2.gsmarena.com/vv/bigpic/apple-iphone-5-ofic.jpg" alt="Phone" /><br>Apple iPhone 5</a></li><li><a class="module-phones-link" href="apple_iphone_6s_plus-7243.php"><img src="http://cdn2.gsmarena.com/vv/bigpic/apple-iphone-6s-plus.jpg" alt="Phone" /><br>Apple iPhone 6s Plus</a></li>
</ul>

<p class="more"><a class="more-news-link" href="apple-phones-48.php">More from Apple</a></p></div>

</aside>

</div><!-- id body -->
</div><!-- id outer -->


<div id="footer">
 <div class="footer-logo">
     <img src="http://cdn2.gsmarena.com/w/css/logo-gsmarena-com.gif" alt="" />
    </div>
    <div id="footmenu">
<p>
<a href="/">Home</a>
<a href="news.php3">News</a>
<a href="reviews.php3">Reviews</a>
<a href="blog.php3">Blog</a>
<a href="compare.php3">Compare</a>
<a href="network-bands.php3">Coverage</a>
<a href="glossary.php3">Glossary</a>
<a href="faq.php3">FAQ</a>

<a href="rss-news-reviews.php3" class="rss-icon">RSS feed</a></li>
<a target="_blank" href="https://www.facebook.com/GSMArenacom-189627474421/" class="fb-icon">Facebook</a>
<a target="_blank" href="http://twitter.com/gsmarena_com" class="tw-icon">Twitter</a>

</p>
<p>
&copy; 2000-2016 <a href="team.php3">GSMArena.com</a>
<a href="http://www.gsmarena.com/switch.php3?ver=mobile&ref=MH5vb3N6QHZvd3BxekApbEBvc2psMigtKywxb3dv">Mobile version</a>
<a href="contact.php3">Contact us</a>
<a href="advert.php3">Advertising</a>
<a href="privacy.php3">Privacy</a>
<a href="terms.php3">Terms of use</a>

</p>
<div id="cdn-hosting">
<a href="http://www.maxcdn.com/" target="_blank" rel="nofollow">
  <span class="center">CDN by</span><br />
  <img src="http://cdn2.gsmarena.com/w/css/maxcdn.gif" alt="" />
</a>
</div>
  </div>
 </div>



</div>


<script>
	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	ga('create', 'UA-131096-1', 'auto' );
	ga('send', 'pageview'
, { 'dimension2': '3' }
	);

</script>

</body></html>
