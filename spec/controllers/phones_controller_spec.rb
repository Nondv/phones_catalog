require 'rails_helper'

RSpec.describe PhonesController, type: :controller do
  context 'index action' do
    before :each do
      get 'index'
    end

    it('exists') { expect(response.status).to eq 200 }

    it('renders view :index') { expect(response).to render_template(:index) }
  end

  context 'brands JSON' do
    before(:each) { get('brands', format: :json) }

    it('exists') { expect(response.status).to eq 200 }
    it('renders json') { expect { JSON.parse(response.body) }.not_to raise_error }
  end


  context 'models JSON' do
    before(:each) do
      get 'models',
          format: :json,
          name: 'Apple',
          gsmarena_url: 'http://www.gsmarena.com/apple-phones-48.php'
    end

    it('exists') { expect(response.status).to eq 200 }
    it('renders json') { expect { JSON.parse(response.body) }.not_to raise_error }
  end


  context 'model_info JSON' do
    before(:each) do
      get 'model_info',
          format: :json,
          name: 'iPhone 6s Plus',
          gsmarena_url: 'http://www.gsmarena.com/apple_iphone_6s_plus-7243.php'
    end

    it('exists') { expect(response.status).to eq 200 }
    it('renders json') { expect { JSON.parse(response.body) }.not_to raise_error }
  end


  context 'search JSON' do
    before(:each) do
      get 'search',
          format: :json,
          query: 'apple iphone'
    end

    it('exists') { expect(response.status).to eq 200 }
    it('renders json') { expect { JSON.parse(response.body) }.not_to raise_error }
  end
end
