angular.module('phones').controller 'PhonesController', ['$scope', '$http', ($scope, $http) ->
  $http
  .get('brands.json')
  .then((res) -> ($scope.brand_list = res.data))

  $scope.update_selected_brand_models = ->
    $scope.selected_brand_models = []
    $scope.selected.model = null
    return unless $scope.selected.brand

    promise = $http
                url: '/models.json',
                method: 'GET',
                params: $scope.selected.brand

    promise.then (response) -> ($scope.selected_brand_models = response.data)

  $scope.update_model_info = ->
    $scope.model_info = {}
    return unless $scope.selected.model

    promise = $http
                url: '/details.json',
                method: 'GET',
                params: $scope.selected.model

    promise.then (response) -> ($scope.model_info = response.data)

  $scope.search_model = ->
    $scope.found_models = []

    promise = $http
                url: '/search.json',
                method: 'GET',
                params: { query: $scope.finder.input }

    promise.then (response) -> ($scope.found_models = response.data)
]
