class Model
  attr_reader :name,
              :gsmarena_url

  def self.find(query)
    Parser.model_search(query)
  end

  def initialize(attributes)
    @name = attributes[:name]
    @gsmarena_url = attributes[:gsmarena_url]
  end

  def info
    Parser.model_info(self)
  end
end
