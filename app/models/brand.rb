class Brand
  attr_reader :name
  attr_reader :gsmarena_url

  def self.all
    Parser.brands
  end

  def initialize(params = {})
    @name = params[:name]
    @gsmarena_url = params[:gsmarena_url]
  end

  def models
    Parser.models(self)
  end
end
