module Parser
  GSMARENA = 'http://www.gsmarena.com'.freeze

  def brands
    brands_from_gsmarena
  end
  module_function :brands

  def models(brand)
    fail 'gsmarena_url is not set' unless brand.gsmarena_url

    models_from_gsmarena(brand.gsmarena_url)
  end
  module_function :models

  def model_info(model)
    fail 'gsmarena_url is not set' unless model.gsmarena_url

    model_info_from_gsmarena(model.gsmarena_url)
  end
  module_function :model_info

  def model_search(query)
    find_model_on_gsmarena(query)
  end
  module_function :model_search

  private

  # TODO: force utf8
  def brands_from_gsmarena
    doc = nokogiri_from_url("#{GSMARENA}/makers.php3")

    table = doc.css('.st-text table')
    link_list = table.css('tr').map { |tr| tr.css('td:nth-child(2n) a') }.flatten
    info = link_list.map do |link|
      { name: link.text.match(/(.*) phones/)[1],
        gsmarena_url: "#{GSMARENA}/#{link['href']}" }
    end

    info.map { |b| Brand.new(b) }
  end
  module_function :brands_from_gsmarena

  # TODO: force utf8
  def models_from_gsmarena(brand_url)
    doc = nokogiri_from_url(brand_url)
    parse_models_grid(doc)
  end
  module_function :models_from_gsmarena

  def model_info_from_gsmarena(url)
    doc = nokogiri_from_url(url)
    spec_tables = doc.css('#specs-list table')

    result = spec_tables.map do |table|
      group_name = force_utf8(table.css('th').text)
      table.css('th').remove # th messes with td so odd/even selectors work incorrectly

      spec_names = table.css('td:nth-child(2n+1)').map(&:text).map(&method(:force_utf8))
      spec_values = table.css('td:nth-child(2n)').map(&:text).map(&method(:force_utf8))
      specs = spec_names.zip(spec_values).to_h

      [group_name, specs]
    end

    result.to_h
  end
  module_function :model_info_from_gsmarena

  def find_model_on_gsmarena(query)
    doc = nokogiri_from_url("#{GSMARENA}/results.php3", params: { sQuickSearch: 'yes', sName: query })
    parse_models_grid(doc)
  end
  module_function :find_model_on_gsmarena

  # Parses
  def nokogiri_from_url(url, params = nil)
    response = RestClient.get(url, params)
    Nokogiri::HTML(response.body)
  end
  module_function :nokogiri_from_url

  #
  # I had some problems with encodings.
  # Simplest way was just to force utf-8
  #
  def force_utf8(str)
    str.encode('utf-8', invalid: :replace, undef: :replace)
  end
  module_function :force_utf8

  def parse_models_grid(doc)
    ul = doc.css('.makers ul')

    info = ul.css('a').map do |a|
      name = a.css('span')
      name.css('br').each { |br| br.replace(' ') }

      { name: name.text,
        gsmarena_url: "#{GSMARENA}/#{a['href']}" }
    end

    info.map { |m| Model.new(m) }
  end
  module_function :parse_models_grid
end
