class PhonesController < ApplicationController
  def index
  end

  def brands
    respond_to do |format|
      format.json { render json: Brand.all }
    end
  end

  def models
    respond_to do |format|
      format.json { render json: Brand.new(brand_params).models }
    end
  end

  def model_info
    respond_to do |format|
      format.json { render json: Model.new(model_params).info }
    end
  end

  def search
    respond_to do |format|
      format.json { render json: Model.find(params[:query]) }
    end
  end

  private

  def brand_params
    params.slice(:name, :gsmarena_url)
  end

  def model_params
    params.slice(:name, :gsmarena_url)
  end
end
